let express = require('express');
let bodyParser = require('body-parser');
let path = require('path');
let expressValidator = require('express-validator');
let app = express();

// var logger = (req,res,next) =>{
//   console.log('Logging...');
//   next();
// };
//
// app.use(logger);

// View engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Set static path
// put static html files in here to serve
app.use(express.static(path.join(__dirname,'public')));

// Global vars
app.use((req,res,next)=>{
  res.locals.errors = null;
  next();
});

// express valdiator middleware
app.use(expressValidator());
// PASS INTO res.json();
let users = [
  {
    id: 1,
    first_name: 'John',
    last_name: 'Dingle',
    email: 'JD@gmail.com'
  },
  {
    id: 2,
    first_name: 'Bohn',
    last_name: 'Fangle',
    email: 'BF@gmail.com'
  }
];

app.get('/',(req,res)=>{
  //res.send();
  //red.json();
  //res.render;

  res.render('index.ejs', {
    'title':'Customers',
    'users':users
  });
});

app.post('/users/add', (req,res)=>{

  req.checkBody('first_name', 'First Name is Required').notEmpty();
  req.checkBody('last_name', 'Last Name is Required').notEmpty();
  req.checkBody('email', 'Email is Required').notEmpty();

  var errors = req.validationErrors();

  if(errors){
    res.render('index', {
      title: 'Customers',
      users: users,
      errors: errors
    });
  }
  else{
    let newUser = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
    };
    console.log(newUser);
  }
});

app.listen(3000, () =>{
  console.log('Server Started on port 3000...');
});
